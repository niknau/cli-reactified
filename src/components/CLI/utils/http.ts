export const getCSV = async (outputFormat: string) => {
  const csvData = await fetch(`http://localhost:8080/csv/${outputFormat}`)
    .then(response => {
      return response.json();
    })
    .then(data => {
      return data;
    });
  return csvData;
};

export const getPRN = async (outputFormat: string) => {
  const prnData = await fetch(`http://localhost:8080/prn/${outputFormat}`)
    .then(response => {
      return response.json();
    })
    .then(data => {
      return data;
    });
  return prnData;
};
