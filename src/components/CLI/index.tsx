import React, { FC } from "react";
import styled from "styled-components";

// CLI component
import Terminal from "terminal-in-react";

// Utils
import { getCSV, getPRN } from "./utils/http";

const CLIContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  position: relative;
  div:first-child {
    margin: auto;
  }
`;

const CLI: FC = () => {
  const handleOnGetCSV = (args: any) => {
    const outputFormat = args._[0];
    getCSV(outputFormat).then(handleOnResponse);
  };

  const handleOnGetPRN = (args: any) => {
    const outputFormat = args._[0];
    getPRN(outputFormat).then(handleOnResponse);
  };

  const handleOnResponse = (response: object) =>
    // Hotfix for "terminal-in-react" not working in strict mode (logging maps to the "watchConsoleLogging" prop
    console.log(JSON.stringify(response));

  return (
    <div>
      <CLIContainer>
        <Terminal
          watchConsoleLogging
          color="green"
          backgroundColor="black"
          barColor="black"
          style={{ fontWeight: "bold", fontSize: "1em" }}
          commands={{
            "get-csv": { method: (args: any) => handleOnGetCSV(args) },
            "get-prn": { method: (args: any) => handleOnGetPRN(args) }
          }}
        />
      </CLIContainer>
    </div>
  );
};

export default CLI;
