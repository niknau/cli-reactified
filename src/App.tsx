import React, { FC } from "react";
import logo from "./logo.svg";
import "./App.css";

// Components
import CLI from "./components/CLI";

const App: FC = () => {
  return (
    <div className="App">
      <CLI />
    </div>
  );
};

export default App;
