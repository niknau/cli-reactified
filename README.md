# CLI Reactified

This applications holds an interface for requesting data (through a terminal) from the server side of the application.

Both front- and back end is included in this repository.

## Steps to run this application

### Clone this repository

Clone this repository running `git clone https://niknau@bitbucket.org/niknau/cli-reactified.git` in your terminal of choice.

### Navigate to the "cli-reactified" folder

### Setup the environment

Setup the environment by running `yarn or npm install in the root directory`.

### Start the application

Start the application by running `yarn start` and (in another tab of your terminal) `yarn start:server`.

### Enabled commands

The commands that are enabled in the terminal are

- get-csv json (to get the json represantation of the .csv file)
- get-prn json (to get the json represantation of the .prn file)

- get-csv html (to get the html represantation of the .csv file)
- get-prn html (to get the html represantation of the .prn file)
