const express = require("express");
const utils = require("./utils");

const app = express();

// CORS configuratio
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// REST
app.get("/csv/json", async function(req, res) {
  utils.handleCSVRequest("json", res);
});

app.get("/csv/html", async function(req, res) {
  utils.handleCSVRequest("html", res);
});

app.get("/prn/json", function(req, res) {
  utils.handlePRNRequest("json", res);
});

app.get("/prn/html", function(req, res) {
  utils.handlePRNRequest("html", res);
});

app.listen(8080, function() {
  console.log("Server listening on port 8080.");
});
