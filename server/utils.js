const fs = require("fs");
const path = require("path");

const XLSX = require("xlsx");
const neatCsv = require("neat-csv");

exports.handleCSVRequest = async (outputFormat, res) => {
  const fileLoc = path.resolve("./server/static/workbook/Workbook2.csv");
  await fs.readFile(fileLoc, { encoding: "latin1" }, async (err, response) => {
    if (err) {
      throw new Error(err);
    }
    return neatCsv(response).then(parsedData => {
      if (outputFormat === "json") {
        res.send(parsedData);
      } else {
        let html = "<div>";
        parsedData.forEach(o => {
          let item = "<div>";
          Object.keys(o).forEach(key => {
            item += `<span>${key}: ${o[key]}</span>`;
          });
          item += "</div>";
          html += item;
        });
        html += "</div>";
        res.send([html]);
      }
    });
  });
};

exports.handlePRNRequest = (outputFormat, res) => {
  const data = [];
  let html = "<div>";

  const fileLoc = path.resolve("./server/static/workbook/Workbook2.prn");
  const workbook = XLSX.readFile(fileLoc, { codepage: 1252 });
  const sheetNameList = workbook.SheetNames;
  sheetNameList.forEach(function(y) {
    const worksheet = workbook.Sheets[y];

    let keys = [];
    let previousItemValues = "";
    for (z in worksheet) {
      if (z[0] === "!") continue;
      let tt = 0;
      for (let i = 0; i < z.length; i++) {
        if (!isNaN(z[i])) {
          tt = i;
          break;
        }
      }
      const col = z.substring(0, tt);
      const row = parseInt(z.substring(tt));

      if (row === 1) {
        const values = worksheet[z].v.replace(/ {2,}/g, ",").split(",");
        keys = [...values];
      } else if (col === "B") {
        const values = worksheet[z].v.replace(/ {2,}/g, ",").split(",");
        values[0] = `${previousItemValues},${values[0]}`;

        if (outputFormat === "json") {
          const dataItem = {};
          for (let i = 0; i < keys.length; i++) {
            dataItem[keys[i]] = values[i];
          }
          data.push(dataItem);
        } else {
          let htmlItem = "<div>";
          for (let i = 0; i < keys.length; i++) {
            htmlItem += `<span>${keys[i]}: ${values[i]}</span>`;
          }
          htmlItem += "</div>";
          html += htmlItem;
        }
      } else {
        previousItemValues = worksheet[z].v;
      }
    }
  });

  if (outputFormat === "html") {
    html + "</div>";
    data.push(html);
  }

  res.send(data);
};
